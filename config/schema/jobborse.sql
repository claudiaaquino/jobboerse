/*
Navicat MySQL Data Transfer

Source Server         : Triade Local
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : jobborse

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2017-10-17 22:37:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for applications
-- ----------------------------
DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `desired_wage` float DEFAULT NULL,
  `hours_weekly` int(11) DEFAULT NULL,
  `motivation` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of applications
-- ----------------------------
INSERT INTO `applications` VALUES ('1', '6', '5', '10', null, 'Isso é um teste de aplicação', '2017-10-17 10:27:16', '2017-10-17 14:32:40', '1');
INSERT INTO `applications` VALUES ('3', '2', '5', '15', '20', 'Outro teste', '2017-10-17 10:36:29', null, '1');
INSERT INTO `applications` VALUES ('4', '3', '1', null, null, 'Qualquer coisa para poder testar', '2017-10-17 10:52:32', null, '1');
INSERT INTO `applications` VALUES ('5', '5', '1', null, '40', 'Ultimo teste de fazer bewerbung', '2017-10-17 10:53:03', null, '1');

-- ----------------------------
-- Table structure for jobqualifications
-- ----------------------------
DROP TABLE IF EXISTS `jobqualifications`;
CREATE TABLE `jobqualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `qualification_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jobqualifications
-- ----------------------------
INSERT INTO `jobqualifications` VALUES ('1', '6', '3');
INSERT INTO `jobqualifications` VALUES ('2', '6', '5');
INSERT INTO `jobqualifications` VALUES ('4', '6', '1');
INSERT INTO `jobqualifications` VALUES ('5', '6', '7');
INSERT INTO `jobqualifications` VALUES ('6', '3', '13');
INSERT INTO `jobqualifications` VALUES ('7', '3', '14');
INSERT INTO `jobqualifications` VALUES ('8', '7', '7');
INSERT INTO `jobqualifications` VALUES ('9', '7', '13');
INSERT INTO `jobqualifications` VALUES ('10', '7', '14');
INSERT INTO `jobqualifications` VALUES ('11', '8', '5');
INSERT INTO `jobqualifications` VALUES ('12', '8', '6');
INSERT INTO `jobqualifications` VALUES ('13', '10', '2');
INSERT INTO `jobqualifications` VALUES ('14', '10', '4');
INSERT INTO `jobqualifications` VALUES ('15', '10', '6');
INSERT INTO `jobqualifications` VALUES ('16', '11', '6');
INSERT INTO `jobqualifications` VALUES ('17', '12', '6');
INSERT INTO `jobqualifications` VALUES ('18', '13', '6');
INSERT INTO `jobqualifications` VALUES ('19', '16', '6');

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unternehmen_id` int(11) NOT NULL,
  `jobtype_id` int(11) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `wage` float DEFAULT NULL,
  `flag_wagehours` tinyint(1) DEFAULT NULL,
  `hours_weekly` int(5) DEFAULT NULL,
  `token` varchar(1024) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES ('1', '1', '4', 'Softwareentwickler (m/w) .NET', 'Wen suchen wir?\r\n\r\n    Durch ein technisches Studium, eine technische Ausbildung und / oder vergleichbare Berufserfahrungen besitzen Sie gute Programmierkenntnisse in C# und ASP.NET MVC.\r\n    Sie konnten bereits relevante Erfahrungen als ASP.Net-Software-Entwickler sammeln.\r\n    Zudem beherrschen Sie HTML4, HTML5, CSS und Javascript genauso wie JQuery und Ajax.\r\n    Sie verfügen über gute Kenntnisse in SQL.\r\n    Ihre Deutsch- und Englisch-Kenntnisse sind sicher in Wort und Schrift.\r\n    Selbstständiges, präzises und effizientes Arbeiten sowie die Lust, Verantwortung zu übernehmen, zeichnen Sie aus.\r\n    Unser Team bereichern Sie durch Ihre sehr guten Kommunikations- und Präsentationsfähigkeiten, Ihr konzeptionelles und strukturiertes Denken, Ihre Kreativität und Flexibilität.\r\n', null, null, null, null, '2017-10-16 19:17:34', null, '1');
INSERT INTO `jobs` VALUES ('2', '1', '6', 'Praktikant (m/w) im Bereich Kommunikationsdesign', 'Wen suchen wir?\r\n\r\n    Deine Stärke liegt im kreativen Design\r\n    Du hast einen Hang zu technischen Themen\r\n    Du bringst mindestens 3–4 Semester Studienerfahrung mit\r\n    Oder 1–2 Jahre Praxiserfahrung\r\n    Und kannst das mit Arbeitsproben belegen\r\n', '450', null, '40', null, '2017-10-16 19:18:59', null, '1');
INSERT INTO `jobs` VALUES ('3', '1', '4', 'Bezahltes Praktikum Ruby Entwicklung Loftbüro Karlsruhe (m/w) ', 'Wen suchen wir?\r\n\r\n    Du bringst Erfahrung von Internet-Technologien und Protokollen, wie z.B. HTML5, XML, CSS3 und JavaScript (jQuery, ..) sowie Bootstrap mit und bist bei neuen Technologien immer am Zahn der Zeit.\r\n    Du kennst dich mit SQL und NoSQL Datenbanken (z.B. MySQL, Redis, MongoDB) und mit den neusten Deployment Werkzeugen und Versionskontrollsystemen wie z.B. Git und Continuous Integration aus.\r\n    Du hast Spaß daran selbständig, akkurat und mit „Clean Codes“ zu arbeiten und verlierst dabei das Ziel, ausgezeichnete User Experience zu gewähren, nie aus den Augen.\r\n    Versionskontrolle liegt Dir im Blut, Du liebst MVC und die dritte Normalform. Test Driven Development hast Du bereits eingesetzt.\r\n    Du überzeugst außerdem durch Teamfähigkeit, eigenständiges und zielgerichtetes Arbeiten und verfügst über gute Kommunikationsfähigkeiten\r\n    Sehr gute Deutschkenntnisse und eine gute Portion Humor runden Dein Profil ab.\r\n    Du hast mind. 6 Monate Zeit für dein Pflichtpraktikum\r\n', null, null, '40', null, '2017-10-16 19:19:50', null, '1');
INSERT INTO `jobs` VALUES ('4', '1', '2', 'Berufseinstieg: Software Entwickler (m/w) ', 'Ihre Aufgaben:\r\n\r\n„ganz einfach - Sie entwickeln gute Software“.Dazu gehört die Anforderungsanalyse mit dem Kunden, die Erstellung von User Stories zur Problembeschreibung und Abstraktion der Lösungen, sowie die sich anschließende Umsetzung in eine moderne Anwendung. Die Software entwickeln Sie in der Programmiersprache JAVA unter Berücksichtigung von Clean-Coding-Standards und Test-driven Development unter Benutzung der Eclipse Entwicklungsumgebung. Auch während der gesamten Umsetzungsphase sind Sie im engen und agilen Kontakt mit Kunden sowie in ständiger Interaktion mit Ihren Teamkollegen. Sie begleiten ein Softwareprojekt von der Idee bis hin zum ausgereiften Produkt. Sie sind bereit Verantwortung zu übernehmen und arbeiten vertrauensvoll, zuverlässig und lösungsorientiert.', null, null, '40', null, '2017-10-16 19:21:03', null, '1');
INSERT INTO `jobs` VALUES ('5', '1', '1', 'Abschlussarbeit mit Übernahme als Java Software-Entwickler (m/w)', 'Wen suchen wir?\r\n\r\n    Für den Standort Karlsruhe suchen wir richtig gute Software-Entwickler (m/w) mit kreativen Ideen, eigenem Antrieb und Spaß an spannender Teamarbeit und bieten Ihnen den Einstieg über eine (Bachelor- oder Master-) Abschlussarbeit an.\r\n    Sie sind Informatiker oder Wirtschaftsinformatiker aus Leidenschaft (FH, TH, Uni) oder haben eine vergleichbare Qualifikation.\r\n    Deutsch und Englisch beherrschen Sie natürlich in Wort und Schrift. \r\n    Mit modernen Werkzeugen und Methoden objektorientiert entwickeln, ist genau Ihr Ding. \r\n    In JAVA oder C / C++ / C# macht Ihnen so schnell keiner was vor. \r\n', null, null, '40', null, '2017-10-16 19:21:50', null, '1');
INSERT INTO `jobs` VALUES ('6', '1', '7', 'Werkstudent im Bereich Softwareentwicklung (m/w) .NET', 'Was solltest du mitbringen?\r\n\r\n    Bu bist eingeschriebener Student in einem informationstechnischen Studiengang \r\n    Du besitzt gute Programmierkenntnisse in C# und ASP.NET MVC.\r\n    Du konntest bereits Erfahrungen im Bereich ASP.Net-Software-Entwicklung sammeln.\r\n    Zudem besitzt Du Kenntnisse HTML4, HTML5, CSS oder Javascript und SQL.\r\n    Selbstständiges, präzises und effizientes Arbeiten zeichnet Dich aus.\r\n', '10', null, '20', null, '2017-10-16 19:37:13', null, '1');
INSERT INTO `jobs` VALUES ('16', '1', '3', 'ultimo teste', 'ultimo teste', '651', null, null, 'a869ccbcbd9568808b8497e28275c7c8', '2017-10-17 16:31:22', null, '1');

-- ----------------------------
-- Table structure for jobtypes
-- ----------------------------
DROP TABLE IF EXISTS `jobtypes`;
CREATE TABLE `jobtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jobtypes
-- ----------------------------
INSERT INTO `jobtypes` VALUES ('1', 'Abschlussarbeit');
INSERT INTO `jobtypes` VALUES ('2', 'Festanstellung');
INSERT INTO `jobtypes` VALUES ('3', 'Pflichtpraktikum');
INSERT INTO `jobtypes` VALUES ('4', 'Praktikum');
INSERT INTO `jobtypes` VALUES ('5', 'Projektarbeit');
INSERT INTO `jobtypes` VALUES ('6', 'Treineeship');
INSERT INTO `jobtypes` VALUES ('7', 'Werkstudententätigkeit');

-- ----------------------------
-- Table structure for qualifications
-- ----------------------------
DROP TABLE IF EXISTS `qualifications`;
CREATE TABLE `qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of qualifications
-- ----------------------------
INSERT INTO `qualifications` VALUES ('1', 'HTML', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('2', 'C#', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('3', 'C++', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('4', 'C', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('5', 'JQuery', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('6', 'Angular', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('7', 'PHP', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('8', 'CSS', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('9', '...', '2017-10-16 21:16:33', null, null);
INSERT INTO `qualifications` VALUES ('10', 'CakePHP', '2017-10-16 18:28:14', null, null);
INSERT INTO `qualifications` VALUES ('11', 'Teste1', '2017-10-16 18:28:29', null, null);
INSERT INTO `qualifications` VALUES ('12', 'Teste2', '2017-10-16 18:28:29', null, null);
INSERT INTO `qualifications` VALUES ('13', 'HTML5', '2017-10-16 19:42:44', null, null);
INSERT INTO `qualifications` VALUES ('14', 'JavaScript', '2017-10-16 19:42:44', null, null);

-- ----------------------------
-- Table structure for unternehmen
-- ----------------------------
DROP TABLE IF EXISTS `unternehmen`;
CREATE TABLE `unternehmen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  `email` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of unternehmen
-- ----------------------------
INSERT INTO `unternehmen` VALUES ('1', 'Firma 1', 'Morbi leo mi, nonummy eget, tristique non, rhoncus non, leo. Nullam faucibus mi quis velit. Integer in sapien. Fusce tellus odio, dapibus id, fermentum quis, suscipit id, erat. Fusce aliquam vestibulum ipsum. Aliquam erat volutpat. Pellentesque sapien. Cras elementum. Nulla pulvinar eleifend sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque porta. Vivamus porttitor turpis ac leo. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit ante. Aliquam erat volutpat. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Duis condimentum augue id magna semper rutrum. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Fusce consectetuer risus a nunc. Aliquam ornare wisi eu metus. Integer pellentesque quam vel velit. Duis pulvinar.\r\n\r\n Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Etiam commodo dui eget wisi. Donec iaculis gravida nulla. Donec quis nibh at felis congue commodo. Etiam bibendum elit eget erat.\r\n\r\n Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu,', 'cavieiraaquino@gmail.com', 'Kaiserstraße  123', '76133 ', 'Deutschland', 'Baden', 'Karlsruhe', '31321351651', '2017-10-16 20:38:16', '2017-10-16 20:39:19', '1');
INSERT INTO `unternehmen` VALUES ('2', 'Firma 2', 'Etiam posuere quam ac quam. Maecenas aliquet accumsan leo. Nullam dapibus fermentum ipsum. Etiam quis quam. Integer lacinia. Nulla est. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Integer vulputate sem a nibh rutrum consequat. Maecenas lorem. Pellentesque pretium lectus id turpis. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Fusce wisi. Phasellus faucibus molestie nisl. Fusce eget urna. Curabitur vitae diam non enim vestibulum interdum. Nulla quis diam. Ut tempus purus at lorem. Morbi leo mi, nonummy eget, tristique non, rhoncus non, leo. Nullam faucibus mi quis velit. Integer in sapien. Fusce tellus odio, dapibus id, fermentum quis, suscipit id, erat. Fusce aliquam vestibulum ipsum. Aliquam erat volutpat. Pellentesque sapien. Cras elementum. Nulla pulvinar eleifend sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque porta. Vivamus porttitor turpis ac leo.\r\n\r\n Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi gravida libero nec velit. Morbi scelerisque luctus velit. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Proin mattis lacinia justo. Vestibulum facilisis auctor urna. Aliquam in lorem sit amet leo accumsan lacinia. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Phasellus et lorem id felis nonummy placerat. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Aenean vel massa quis mauris vehicula lacinia. Quisque tincidunt scelerisque libero. Maecenas libero. Etiam dictum tincidunt diam. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Suspendisse nisl. Sed convallis', 'cavieiraaquino@gmail.com', 'Kaiserstraße  456', '76133 ', 'Deutschland', 'Baden', 'Karlsruhe', '31321351651', '2017-10-16 20:38:16', '2017-10-16 20:39:28', '1');

-- ----------------------------
-- Table structure for userqualifications
-- ----------------------------
DROP TABLE IF EXISTS `userqualifications`;
CREATE TABLE `userqualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `qualification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of userqualifications
-- ----------------------------
INSERT INTO `userqualifications` VALUES ('1', '1', '2');
INSERT INTO `userqualifications` VALUES ('3', '1', '5');
INSERT INTO `userqualifications` VALUES ('5', '1', '10');
INSERT INTO `userqualifications` VALUES ('7', '1', '12');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unternehmen_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `cellphone` varchar(20) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', null, 'Bewerber', 'bewerber1', '$2y$10$2aRIQ1nkPWwDlhtUsg8EAeTnfY/4PeJtMSQaL8Du.dcCFc.8.FT7y', 'cavieiraaquino@gmail.com', '', '', '', '', '', '', '', '2017-10-17 10:52:08', '2017-10-14 23:11:15', '2017-10-17 14:52:08', '1');
INSERT INTO `users` VALUES ('2', '1', 'Unternehmen 1', 'firma1', '$2y$10$2aRIQ1nkPWwDlhtUsg8EAeTnfY/4PeJtMSQaL8Du.dcCFc.8.FT7y', 'cavieiraaquino@gmail.com', '', '', '', '', '', '', '', '2017-10-17 10:54:43', '2017-10-14 23:11:15', '2017-10-17 14:54:43', '1');
INSERT INTO `users` VALUES ('3', '2', 'Unternehmen 1', 'firma2', '$2y$10$2aRIQ1nkPWwDlhtUsg8EAeTnfY/4PeJtMSQaL8Du.dcCFc.8.FT7y', 'cavieiraaquino@gmail.com', '', '', '', '', '', '', '', '2017-10-14 18:23:33', '2017-10-14 23:11:15', '2017-10-16 20:33:22', '1');
INSERT INTO `users` VALUES ('5', null, 'Bewerber2', 'bewerber2', '$2y$10$724G87ffCF6syS8aDzYOROIBR0JgD..bta2utW8Mz46iagpVGsfKO', 'teste@teste.com', null, null, null, null, null, null, null, '2017-10-17 09:48:30', '2017-10-17 09:48:18', '2017-10-17 14:55:32', '1');
