<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $paginate = [
        'limit' => 10
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'view'
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'finder' => 'auth'
                ]
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'authError' => 'unauthorized access',
            'storage' => 'Session'
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        if ($this->request->session()->read('Auth.User.id')) {
            $this->viewBuilder()->layout('index');

            $name = $this->request->session()->read('Auth.User.name');
            $this->set("loggednome", $name);

            $loggedfirstname = explode(' ', $name);
            $this->set("loggedfirstname", $loggedfirstname[0]);

            $this->set("unternehmen_id", $this->request->session()->read('Auth.User.unternehmen_id'));
            $this->set("backlink", $this->referer());
            $this->set("loggeduser", $this->request->session()->read('Auth.User.id'));
            $this->set("loggedIn", true);
        } else {
            $this->set("loggedIn", false);
        }
    }

    public function beforeFilter(Event $event) {
        if (!$this->Auth->user('id')) {
            $this->Auth->config('authError', false);
        }
    }

    public function directlogin($user) {
        $this->Auth->setUser($user);
    }

}
