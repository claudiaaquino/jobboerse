<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Applications Controller
 *
 * @property \App\Model\Table\ApplicationsTable $Applications
 */
class ApplicationsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Jobs.Unternehmen', 'Users']
        ];

        if ($this->Auth->user('unternehmen_id')) {
            $query = $this->Applications->find()->innerJoinWith('Jobs')->where(['Jobs.unternehmen_id' => $this->Auth->user('unternehmen_id')]);
        } else {
            $query = $this->Applications->find()->where(['Applications.user_id' => $this->Auth->user('id')]);
        }


        $applications = $this->paginate($query);

        $this->set(compact('applications'));
        $this->set('_serialize', ['applications']);
    }

    /**
     * View method
     *
     * @param string|null $id Application id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $application = $this->Applications->get($id, [
            'contain' => ['Jobs', 'Users']
        ]);

        $this->set('application', $application);
        $this->set('_serialize', ['application']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($job_id = null) {
        $application = $this->Applications->newEntity();
        
        $application->job_id = $job_id ? $job_id : $application->job_id;

        if ($this->request->is('post')) {
            $application = $this->Applications->patchEntity($application, $this->request->data);
            $application->created = date('Y-m-d H:i:s');
            $application->user_id = $this->Auth->user('id');
            if ($this->Applications->save($application)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }
        
        $this->set(compact('application'));
        $this->set('_serialize', ['application']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Application id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $application = $this->Applications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $application = $this->Applications->patchEntity($application, $this->request->data);
            if ($this->Applications->save($application)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }
        $jobs = $this->Applications->Jobs->find('list', ['limit' => 200]);
        $users = $this->Applications->Users->find('list', ['limit' => 200]);
        $this->set(compact('application', 'jobs', 'users'));
        $this->set('_serialize', ['application']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Application id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $application = $this->Applications->get($id);
        if ($this->Applications->delete($application)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occurred, please try later.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
