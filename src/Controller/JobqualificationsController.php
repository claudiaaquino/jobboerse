<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Jobqualifications Controller
 *
 * @property \App\Model\Table\JobqualificationsTable $Jobqualifications
 */
class JobqualificationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Jobs', 'Qualifications']
        ];
        $jobqualifications = $this->paginate($this->Jobqualifications);

        $this->set(compact('jobqualifications'));
        $this->set('_serialize', ['jobqualifications']);
    }

    /**
     * View method
     *
     * @param string|null $id Jobqualification id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $jobqualification = $this->Jobqualifications->get($id, [
            'contain' => ['Jobs', 'Qualifications']
        ]);

        $this->set('jobqualification', $jobqualification);
        $this->set('_serialize', ['jobqualification']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $jobqualification = $this->Jobqualifications->newEntity();
        if ($this->request->is('post')) {
            $jobqualification = $this->Jobqualifications->patchEntity($jobqualification, $this->request->data);
            $jobqualification->created =  date('Y-m-d H:i:s');
            $jobqualification->user_id =  $this->Auth->user('id');
            if ($this->Jobqualifications->save($jobqualification)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }
        $jobs = $this->Jobqualifications->Jobs->find('list', ['limit' => 200]);
        $qualifications = $this->Jobqualifications->Qualifications->find('list', ['limit' => 200]);
        $this->set(compact('jobqualification', 'jobs', 'qualifications'));
        $this->set('_serialize', ['jobqualification']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Jobqualification id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $jobqualification = $this->Jobqualifications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $jobqualification = $this->Jobqualifications->patchEntity($jobqualification, $this->request->data);
            if ($this->Jobqualifications->save($jobqualification)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }
        $jobs = $this->Jobqualifications->Jobs->find('list', ['limit' => 200]);
        $qualifications = $this->Jobqualifications->Qualifications->find('list', ['limit' => 200]);
        $this->set(compact('jobqualification', 'jobs', 'qualifications'));
        $this->set('_serialize', ['jobqualification']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Jobqualification id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $jobqualification = $this->Jobqualifications->get($id);
        if ($this->Jobqualifications->delete($jobqualification)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occured, please try later.'));
        }

        return $this->redirect($this->referer());
    }
}
