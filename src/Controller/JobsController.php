<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Event\Event;

/**
 * Jobs Controller
 *
 * @property \App\Model\Table\JobsTable $Jobs
 */
class JobsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Unternehmen', 'Jobtypes']
        ];
        if ($this->Auth->user('unternehmen_id')) {
            $query = $this->Jobs->find()->where(['unternehmen_id' => $this->Auth->user('unternehmen_id')])->orderDesc('Jobs.created');
        } else {
            $query = $this->Jobs->find()->orderDesc('Jobs.created');
        }

        $jobs = $this->paginate($query);

        $this->set(compact('jobs'));
        $this->set('_serialize', ['jobs']);
    }

    /**
     * View method
     *
     * @param string|null $id Job id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $job = $this->Jobs->get($id, [
            'contain' => ['Unternehmen', 'Jobtypes', 'Applications.Users', 'Jobqualifications.Qualifications']
        ]);

        $this->set('job', $job);
        $this->set('_serialize', ['job']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $job = $this->Jobs->newEntity();
        if ($this->request->is('post')) {
            $job = $this->Jobs->patchEntity($job, $this->request->data);
            $job->created = date('Y-m-d H:i:s');
            $job->unternehmen_id = $this->Auth->user('unternehmen_id');


            //the user can select one register or create a new one
            $jobqualifications = array();
            if ($job->qualification_ids) {//if the user selected some qualifications
                foreach ($job->qualification_ids as $qualification) {
                    if (is_numeric($qualification)) {// if the user selected one 
                        $jobqualification = $this->Jobs->Jobqualifications->newEntity();
                        $jobqualification->qualification_id = $qualification;
                        $jobqualifications[] = $jobqualification;
                    } else if (is_string($qualification)) {// is the user created another specific qualification
                        $newqualification = $this->Jobs->Jobqualifications->Qualifications->newEntity();
                        $newqualification->description = $qualification;
                        if ($this->Jobs->Jobqualifications->Qualifications->save($newqualification)) {
                            $jobqualification = $this->Jobs->Jobqualifications->newEntity();
                            $jobqualification->qualification_id = $newqualification->id;
                            $jobqualifications[] = $jobqualification;
                        }
                    }
                }
            }
            $job->jobqualifications = $jobqualifications;

            //it creates the token 
            $job->token = md5($this->Auth->user('id') . $this->Auth->user('unternehmen_id') . date('i'));


            if ($this->Jobs->save($job)) {
                //send a email to the company, with the token 
                $this->emailNewJob($job);

                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }
        $this->loadModel('Qualifications');
        $qualifications = $this->Qualifications->find('list');
        $jobtypes = $this->Jobs->Jobtypes->find('list');
        $this->set(compact('job', 'jobtypes', 'qualifications'));
        $this->set('_serialize', ['job']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Job id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $job = $this->Jobs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $job = $this->Jobs->patchEntity($job, $this->request->data);


            //if the user added or deleted a qualification, then ajust it on DB
            //the user can select one register or create a new one
            $qualifications = $this->Jobs->Jobqualifications->find()->where(['job_id' => $id])->extract('qualification_id')->filter()->toArray();
            $jobqualifications = array();
            if ($job->qualification_ids) {//if the user selected some qualifications
                foreach ($job->qualification_ids as $qualification) {
                    if (!in_array($qualification, $qualifications)) {
                        if (is_numeric($qualification)) {// if the user selected one 
                            $jobqualification = $this->Jobs->Jobqualifications->newEntity();
                            $jobqualification->qualification_id = $qualification;
                            $jobqualifications[] = $jobqualification;
                        } else if (is_string($qualification)) {// is the user created another specific qualification
                            $newqualification = $this->Jobs->Jobqualifications->Qualifications->newEntity();
                            $newqualification->description = $qualification;
                            if ($this->Jobs->Jobqualifications->Qualifications->save($newqualification)) {
                                $jobqualification = $this->Jobs->Jobqualifications->newEntity();
                                $jobqualification->qualification_id = $newqualification->id;
                                $jobqualifications[] = $jobqualification;
                            }
                        }
                    }
                }
                $this->Jobs->Jobqualifications->deleteAll(['job_id' => $id, 'qualification_id NOT IN' => $job->qualification_ids]);
            } else {
                $this->Jobs->Jobqualifications->deleteAll(['job_id' => $id]);
            }
            $job->jobqualifications = $jobqualifications;


            if ($this->Jobs->save($job)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }


        $jobtypes = $this->Jobs->Jobtypes->find('list');

        $this->loadModel('Qualifications');
        $qualifications = $this->Qualifications->find('list');
        $selectedqualifications = array_keys($this->Qualifications->find('list')->innerJoinWith('Jobqualifications')->where(['Jobqualifications.job_id' => $id])->toArray());


        $this->set(compact('job', 'selectedqualifications', 'qualifications', 'jobtypes'));
        $this->set('_serialize', ['job']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Job id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $job = $this->Jobs->get($id);
        if ($this->Jobs->delete($job)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occurred, please try later.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //send a email to the company, damit es den job bearbeiten und löschen kann.
    public function emailNewJob($job) {
        try {
            $email = new Email('default');
            $email->viewVars(['name' => $this->Auth->user('name'), 'jobtitle' => $job->title, 'token' => $job->token]);
            $email->subject('ein neuer Job wurde erfolgreich angelegt')
                    ->template('default', 'newjob')
                    ->emailFormat('html')
                    ->to($this->Auth->user('email'))
                    ->send();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['directaccess']);
    }

    /**
     * Directaccess method
     *
     * @param string|null $token Job token.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function directaccess($token = null) {
        $job = $this->Jobs->find()->where(['token' => $token])->first();
        $user = $this->Jobs->Unternehmen->Users->find()->where(['Users.unternehmen_id' => $job->unternehmen_id])->first();

        if ($job) {
            $this->directlogin($user);
            return $this->redirect(['controller' => 'Jobs', 'action' => 'view', $job->id]);
        } else {
            return $this->redirect(['controller' => 'Users', 'action' => 'index']);
        }
    }

}
