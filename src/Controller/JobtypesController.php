<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Jobtypes Controller
 *
 * @property \App\Model\Table\JobtypesTable $Jobtypes
 */
class JobtypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $jobtypes = $this->paginate($this->Jobtypes);

        $this->set(compact('jobtypes'));
        $this->set('_serialize', ['jobtypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Jobtype id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $jobtype = $this->Jobtypes->get($id, [
            'contain' => ['Jobs']
        ]);

        $this->set('jobtype', $jobtype);
        $this->set('_serialize', ['jobtype']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $jobtype = $this->Jobtypes->newEntity();
        if ($this->request->is('post')) {
            $jobtype = $this->Jobtypes->patchEntity($jobtype, $this->request->data);
            $jobtype->created =  date('Y-m-d H:i:s');
            $jobtype->user_id =  $this->Auth->user('id');
            if ($this->Jobtypes->save($jobtype)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }
        $this->set(compact('jobtype'));
        $this->set('_serialize', ['jobtype']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Jobtype id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $jobtype = $this->Jobtypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $jobtype = $this->Jobtypes->patchEntity($jobtype, $this->request->data);
            if ($this->Jobtypes->save($jobtype)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }
        $this->set(compact('jobtype'));
        $this->set('_serialize', ['jobtype']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Jobtype id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $jobtype = $this->Jobtypes->get($id);
        if ($this->Jobtypes->delete($jobtype)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occurred, please try later.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
