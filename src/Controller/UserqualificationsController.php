<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Userqualifications Controller
 *
 * @property \App\Model\Table\UserqualificationsTable $Userqualifications
 */
class UserqualificationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Qualifications']
        ];
        $userqualifications = $this->paginate($this->Userqualifications);

        $this->set(compact('userqualifications'));
        $this->set('_serialize', ['userqualifications']);
    }

    /**
     * View method
     *
     * @param string|null $id Userqualification id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userqualification = $this->Userqualifications->get($id, [
            'contain' => ['Users', 'Qualifications']
        ]);

        $this->set('userqualification', $userqualification);
        $this->set('_serialize', ['userqualification']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userqualification = $this->Userqualifications->newEntity();
        if ($this->request->is('post')) {
            $userqualification = $this->Userqualifications->patchEntity($userqualification, $this->request->data);
            $userqualification->created =  date('Y-m-d H:i:s');
            $userqualification->user_id =  $this->Auth->user('id');
            if ($this->Userqualifications->save($userqualification)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }
        $users = $this->Userqualifications->Users->find('list', ['limit' => 200]);
        $qualifications = $this->Userqualifications->Qualifications->find('list', ['limit' => 200]);
        $this->set(compact('userqualification', 'users', 'qualifications'));
        $this->set('_serialize', ['userqualification']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Userqualification id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userqualification = $this->Userqualifications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userqualification = $this->Userqualifications->patchEntity($userqualification, $this->request->data);
            if ($this->Userqualifications->save($userqualification)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }
        $users = $this->Userqualifications->Users->find('list', ['limit' => 200]);
        $qualifications = $this->Userqualifications->Qualifications->find('list', ['limit' => 200]);
        $this->set(compact('userqualification', 'users', 'qualifications'));
        $this->set('_serialize', ['userqualification']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Userqualification id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userqualification = $this->Userqualifications->get($id);
        if ($this->Userqualifications->delete($userqualification)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occured, please try later.'));
        }

        return $this->redirect($this->referer());
    }
}
