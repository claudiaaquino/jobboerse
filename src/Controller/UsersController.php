<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Unternehmen']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * Candidates method
     *
     * @return \Cake\Network\Response|null
     */
    public function candidates() {
        $this->paginate = [
            'contain' => ['Userqualifications']
        ];

        //show only normal users
        $users = $this->paginate($this->Users->find()->where(['unternehmen_id is null']));
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);

        $this->render('index');
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        if ($id == null) { //get the user on session
            $id = $this->Auth->user('id');
        }

        $user = $this->Users->get($id, [
            'contain' => ['Unternehmen', 'Applications.Jobs.Unternehmen', 'Userqualifications.Qualifications']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->created = date('Y-m-d H:i:s');
            $user->user_id = $this->Auth->user('id');
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }
        $unternehmen = $this->Users->Unternehmen->find('list', ['limit' => 200]);
        $this->set(compact('user', 'unternehmen'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);

            //if the user added or deleted a qualification, then ajust it on DB
            //the user can select one register or create a new one
            $qualifications = $this->Users->Userqualifications->find()->where(['user_id' => $id])->extract('qualification_id')->filter()->toArray();
            $userqualifications = array();
            if ($user->qualification_ids) {//if the user selected some qualifications
                foreach ($user->qualification_ids as $qualification) {
                    if (!in_array($qualification, $qualifications)) {
                        if (is_numeric($qualification)) {// if the user selected one 
                            $userqualification = $this->Users->Userqualifications->newEntity();
                            $userqualification->qualification_id = $qualification;
                            $userqualifications[] = $userqualification;
                        } else if (is_string($qualification)) {// is the user created another specific qualification
                            $newqualification = $this->Users->Userqualifications->Qualifications->newEntity();
                            $newqualification->description = $qualification;
                            if ($this->Users->Userqualifications->Qualifications->save($newqualification)) {
                                $userqualification = $this->Users->Userqualifications->newEntity();
                                $userqualification->qualification_id = $newqualification->id;
                                $userqualifications[] = $userqualification;
                            }
                        }
                    }
                }
                $this->Users->Userqualifications->deleteAll(['user_id' => $id, 'qualification_id NOT IN' => $user->qualification_ids]);
            } else {
                $this->Users->Userqualifications->deleteAll(['user_id' => $id]);
            }
            $user->userqualifications = $userqualifications;

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }

        $this->loadModel('Qualifications');
        $qualifications = $this->Qualifications->find('list');
        $selectedqualifications = array_keys($this->Qualifications->find('list')->innerJoinWith('Userqualifications')->where(['Userqualifications.user_id' => $id])->toArray());

        $this->set(compact('user', 'qualifications', 'selectedqualifications'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occurred, please try later.'));
        }

        return $this->redirect($this->referer());
    }

    public function login() {

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->registraLogin($this->Auth->user('id'));
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid Username and/or Password'));
        }
        $user = $this->Users->newEntity();
        $this->set('user', $user);
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'login', 'logout', 'esquecisenha', 'redefinirsenha']);
    }

    public function registraLogin($userid) {
        $user = $this->Users->get($userid);
        $user->lastlogin = date('Y-m-d H:i:s');
        $this->Users->save($user);
    }

   

}
