<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Job Entity
 *
 * @property int $id
 * @property int $unternehmen_id
 * @property int $jobtype_id
 * @property string $title
 * @property string $description
 * @property float $wage
 * @property bool $flag_wagehours
 * @property int $hours_weekly
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 * @property bool $status
 *
 * @property \App\Model\Entity\Unternehmen $unternehmen
 * @property \App\Model\Entity\Jobtype $jobtype
 * @property \App\Model\Entity\Application[] $applications
 * @property \App\Model\Entity\Jobqualification[] $jobqualifications
 */
class Job extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
