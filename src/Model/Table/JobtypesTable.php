<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Jobtypes Model
 *
 * @property \Cake\ORM\Association\HasMany $Jobs
 *
 * @method \App\Model\Entity\Jobtype get($primaryKey, $options = [])
 * @method \App\Model\Entity\Jobtype newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Jobtype[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Jobtype|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Jobtype patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Jobtype[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Jobtype findOrCreate($search, callable $callback = null)
 */
class JobtypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('jobtypes');
        $this->displayField('description');
        $this->primaryKey('id');

        $this->hasMany('Jobs', [
            'foreignKey' => 'jobtype_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('description');

        return $validator;
    }
}
