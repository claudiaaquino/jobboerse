<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Unternehmen Model
 * 
 * @property \Cake\ORM\Association\HasOne $Users
 * @property \Cake\ORM\Association\HasMany $Jobs
 *
 * @method \App\Model\Entity\Unternehmen get($primaryKey, $options = [])
 * @method \App\Model\Entity\Unternehmen newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Unternehmen[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Unternehmen|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Unternehmen patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Unternehmen[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Unternehmen findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UnternehmenTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('unternehmen');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        
         $this->hasMany('Jobs', [
            'foreignKey' => 'unternehmen_id'
        ]);
         $this->hasOne('Users', [
            'foreignKey' => 'unternehmen_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('address');

        $validator
            ->allowEmpty('zipcode');

        $validator
            ->allowEmpty('country');

        $validator
            ->allowEmpty('state');

        $validator
            ->allowEmpty('city');

        $validator
            ->allowEmpty('phone');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
   /* public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }*/
}
