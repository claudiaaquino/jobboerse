<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Bewerbung') ?> <small>* required</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($application, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="desired_wage">gewünschter Gehalt <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                         <?= $this->Form->input('desired_wage', ['type' => 'text', "class" => "form-control money col-md-7 col-xs-12", 'label' => false, 'placeholder' => 'Lassen Sie leer, wenn es verhandelbar ist.', 'min' => 0]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours_weekly">Wochenstundenanzahl <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('hours_weekly', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => 'Lassen Sie leer, wenn es verhandelbar ist.', 'min' => 0]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="motivation">Motivation <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('motivation', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Bewerben!</button>
                        <?= $this->Html->link(__('zurück'), $backlink, ['class' => "btn btn-default"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(":input").inputmask();
        $('.money').inputmask('decimal', {
            radixPoint: ".",
            groupSeparator: ",",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});
    });
</script>