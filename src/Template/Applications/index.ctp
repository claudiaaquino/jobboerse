
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?= __('Bewerbungen') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('job_id', 'Job Titel') ?></th>
                                <?php if ($unternehmen_id) { //only a company can see this column       ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('user_id', 'Bewerber') ?></th>
                                <?php } else { ?>
                                    <th scope="col" class="column-title">Unternehmen</th>
                                <?php } ?>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('desired_wage', 'gewünschter Gehalt') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('hours_weekly', 'Wochenstundenanzahl') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('created') ?></th>

                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($applications as $application) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $application->id ?>">
                                    </td>
                                    <td><?= $application->has('job') ? $this->Html->link($application->job->title, ['controller' => 'Jobs', 'action' => 'view', $application->job->id]) : '' ?></td>
                                    <?php if ($unternehmen_id) { //only a company can see this column       ?>
                                        <td><?= $application->has('user') ? $this->Html->link($application->user->name, ['controller' => 'Users', 'action' => 'view', $application->user->id]) : '' ?></td>
                                    <?php } else { ?>
                                        <td><?= $application->has('job') ? $this->Html->link($application->job->unternehman->name, ['controller' => 'Unternehmen', 'action' => 'view', $application->job->unternehman->id]) : '' ?></td>
                                    <?php } ?>
                                    <td><?= $application->desired_wage ? $this->Number->format($application->desired_wage) : 'Verhandelbar' ?></td>
                                    <td><?= $application->hours_weekly ? $this->Number->format($application->hours_weekly) : 'Verhandelbar' ?></td>
                                    <td><?= h($application->created) ?></td>
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('View'), ['action' => 'view', $application->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?php if (!$unternehmen_id) { //only a bewerber can see this options       ?>
                                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $application->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $application->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $application->id)]) ?>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
