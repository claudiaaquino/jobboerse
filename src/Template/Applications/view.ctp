<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Application - COD <?= h($application->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Bewerber') ?></p>
                        <p><?= $application->has('user') ? $this->Html->link($application->user->name, ['controller' => 'Users', 'action' => 'view', $application->user->id]) : 'keine Information' ?></p>




                        <p class="title"><?= __('Desired Wage') ?></p>
                        <p><?= $application->desired_wage ? $this->Number->format($application->desired_wage) . '€' : 'Verhandelbar'; ?></p>


                        <p class="title"><?= __('Hours Weekly') ?></p>
                        <p><?= $application->hours_weekly ? $this->Number->format($application->hours_weekly) . ' Stunden' : 'Verhandelbar'; ?></p>


                        <p class="title"><?= __('Created') ?></p>
                        <p><?= $application->created ? $application->created : 'keine Information'; ?></p>


                        <p class="title"><?= __('Updated') ?></p>
                        <p><?= $application->updated ? $application->updated : 'keine Information'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $application->status ? __('aktiv') : __('Inaktiv'); ?></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Job') ?></p>
                        <p><?= $application->has('job') ? $this->Html->link($application->job->title, ['controller' => 'Jobs', 'action' => 'view', $application->job->id]) : 'keine Information' ?></p>


                        <p class="title"><?= __('Motivation') ?></p>
                        <p>  <?= $application->motivation ? $this->Text->autoParagraph(h($application->motivation)) : 'keine Information'; ?></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("zurück", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        <?php if (!$unternehmen_id) { //only a bewerber can see this options       ?>
                            <?= $this->Html->link("ändern", ['action' => 'edit', $application->id], ['class' => "btn btn-primary"]) ?>
                            <?= $this->Form->postLink("löschen", ['action' => 'delete', $application->id], ['class' => "btn btn-danger", 'confirm' => __('Sind sie sicher, dass Sie es löschen möchten??', $application->id)]) ?>
                        <?php } else { ?>
                            <?= $this->Html->link('Bewerber zeigen', ['controller' => 'Users', 'action' => 'view', $application->user->id], ['class' => "btn btn-primary"]) ?>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


