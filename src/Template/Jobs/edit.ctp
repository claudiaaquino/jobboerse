<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Job ändern') ?> <small>* required</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($job, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>


                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jobtype_id">Beschäftigungsverhältnis <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('jobtype_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $jobtypes, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Job Title <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('title', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Beschreibung <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('description', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="wage">Gehalt (€)
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('wage', ['type' => 'text', "class" => "form-control money col-md-7 col-xs-12", 'label' => false, 'placeholder' => 'Lassen Sie leer, wenn es verhandelbar ist.', 'min' => 0]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours_weekly">Wochenstundenanzahl
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('hours_weekly', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => 'Lassen Sie leer, wenn es verhandelbar ist.', 'min' => 0]); ?>

                    </div> 
                </div>                         

                <div class="form-group well-sm well">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qualification_ids">Anforderungen (Kenntnisse und Fähigkeiten)
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('qualification_ids', ['options' => $qualifications, 'default' => $selectedqualifications, "class" => "form-control", "multiple" => "multiple", 'label' => false, "style" => "width: 100%"]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Job aktualisieren</button>
                        <?= $this->Html->link(__('zurück'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2();
        $('.money').inputmask('decimal', {
            radixPoint: ".",
            groupSeparator: ",",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});
        $("#qualification-ids").select2({tokenSeparators: [',', ';'], tags: true, multiple: true, placeholder: 'wenn es hier nicht gibt, schreib es einfach.'});
    });
</script>