<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?= __('Jobs') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if ($unternehmen_id) { ?>
                        <li><?= $this->Html->link(__('  Neuer Job erstellen'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></li>
                    <?php } ?>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('id') ?></th>
                                <?php if (!$unternehmen_id) { //only a normal user can see this column  ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('unternehmen_id', 'Unternehmen') ?></th>
                                <?php } ?>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('jobtype_id', 'Type') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('title', 'Bezeichnung') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('wage', 'Gehalt') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('hours_weekly', 'Wochenstundenanzahl') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('created') ?></th>

                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($jobs as $job) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $job->id ?>">
                                    </td>

                                    <td><?= $this->Number->format($job->id) ?></td>
                                    <?php if (!$unternehmen_id) { //only a normal user can see this column  ?>
                                        <td><?= $job->unternehman ? $this->Html->link($job->unternehman->name, ['controller' => 'Unternehmen', 'action' => 'view', $job->unternehman->id]) : '' ?></td>
                                    <?php } ?>
                                    <td><?= $job->has('jobtype') ? $this->Html->link($job->jobtype->description, ['controller' => 'Jobtypes', 'action' => 'view', $job->jobtype->id]) : '' ?></td>
                                    <td><?= h($job->title) ?></td>
                                    <td><?= $job->wage ? $this->Number->format($job->wage) : 'Verhandelbar' ?></td>
                                    <td><?= $job->hours_weekly ? $this->Number->format($job->hours_weekly) : 'Verhandelbar' ?></td>
                                    <td><?= h($job->created) ?></td>
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('View'), ['action' => 'view', $job->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?php if ($unternehmen_id) { //only a company has this options  ?>                                          
                                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $job->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $job->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Sind Sie sicher?', $job->id)]) ?>
                                            <?php } else { ?>
                                                <?= $this->Html->link(__('Bewerben'), ['controller' => 'Applications', 'action' => 'add', $job->id], ['class' => "btn btn-info btn-xs"]) ?>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
