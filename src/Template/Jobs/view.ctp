<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Job - <?= h($job->title) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Id') ?></p>
                        <p><?= $job->id ? $this->Number->format($job->id) : 'keine Information'; ?></p>


                        <p class="title"><?= __('Unternehmen') ?></p>
                        <!--ich weiß, es ist falsh geschrieben, aber leider generiert der framwork  automatisch diesen name, wenn das Program läuft.-->
                        <p><?= $job->has('unternehman') ? $this->Html->link($job->unternehman->name, ['controller' => 'Unternehmen', 'action' => 'view', $job->unternehman->id]) : 'keine Information' ?></p>

                        <p class="title"><?= __('Type') ?></p>
                        <p><?= $job->has('jobtype') ? $this->Html->link($job->jobtype->description, ['controller' => 'Jobtypes', 'action' => 'view', $job->jobtype->id]) : 'keine Information' ?></p>



                        <p class="title"><?= __('Gehalt') ?></p>
                        <p><?= $job->wage ? $this->Number->format($job->wage) : 'Verhandelbar'; ?></p>


                        <p class="title"><?= __('Wochenstundenanzahl') ?></p>
                        <p><?= $job->hours_weekly ? $this->Number->format($job->hours_weekly) : 'Verhandelbar'; ?></p>


                        <p class="title"><?= __('Erstellungsdatum') ?></p>
                        <p><?= $job->created ? $job->created : 'keine Information'; ?></p>


                        <p class="title"><?= __('Änderungsdatum') ?></p>
                        <p><?= $job->updated ? $job->updated : 'keine Information'; ?></p>

                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $job->status ? __('aktiv') : __('Inaktiv'); ?></p>

                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Title') ?></p>
                        <p><?= $job->title ? $job->title : 'keine Information'; ?></p>

                        <p class="title"><?= __('Beschreibung') ?></p>
                        <p><?= $job->description ? $job->description : 'keine Information'; ?></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("zurück", $backlink, ['class' => "btn btn-default"]) ?>
                        <?php if ($unternehmen_id) { //only a company has this options  ?>
                            <?= $this->Html->link("ändern", ['action' => 'edit', $job->id], ['class' => "btn btn-primary"]) ?>
                            <?= $this->Form->postLink("löschen", ['action' => 'delete', $job->id], ['class' => "btn btn-danger", 'confirm' => __('Sind sie sicher, dass Sie es löschen möchten??', $job->id)]) ?>
                        <?php } else { ?>
                            <?= $this->Html->link("Bewerben", ['controller' => 'Applications', 'action' => 'add', $job->id], ['class' => "btn btn-primary"]) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($job->jobqualifications)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Anforderungen </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Qualifikation') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($job->jobqualifications as $jobqualifications):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($jobqualifications->qualification->description) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?php if ($unternehmen_id) { ?>
                                                    <?= $this->Form->postLink(__('löschen'), ['controller' => 'Jobqualifications', 'action' => 'delete', $jobqualifications->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Sind sie sicher, dass Sie es löschen möchten??', $jobqualifications->id)]) ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>


