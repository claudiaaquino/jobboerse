<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Jobbörse</title>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->Html->script('/vendors/jquery/dist/jquery.min.js', array('inline' => false)) //necessário para carregas os outros js dentro dos content's ?>
        <?= $this->Html->script('/js/ajax/index.js', array('inline' => false)); ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?> 

        <style type="text/css">
            body a:hover {
                color: #194f8c;
            }
        </style>

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <span class="site_title">
                                Jobbörse
                            </span>
                        </div>

                        <div class="clearfix"></div>

                        <!--menu profile quick info--> 
                        <div class="profile">
                            <div class="profile_pic">
                                <?php echo $this->Html->image("profiles/default.png", ["height" => 70, "class" => 'img-circle profile_img', 'url' => ['controller' => 'Home', 'action' => 'index']]); ?>
                            </div>
                            <div class="profile_info">
                                <span>Hallo,</span>
                                <h2><?= $loggednome ?></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info 

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>Menu</h3>
                                <ul class="nav side-menu">
                                    <!--para cada menu-->
                                    <li><a><i class="fa fa-briefcase"></i> Jobs <span class="fa fa-chevron-down"></span></a>
                                        <!--para cada submenu-->
                                        <ul class="nav child_menu">
                                            <li><?= $this->Html->link('Add Job', ['controller' => 'Jobs', 'action' => 'add']); ?></li>
                                        </ul>
                                        <ul class="nav child_menu">
                                            <li><?= $this->Html->link('My Jobs', ['controller' => 'Jobs', 'action' => 'index']); ?></li>
                                        </ul>
                                        <ul class="nav child_menu">
                                            <li><?= $this->Html->link('Applications', ['controller' => 'Applications', 'action' => 'index']); ?></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-group"></i> Candidates <span class="fa fa-chevron-down"></span></a>

                                        <!--para cada submenu-->
                                        <ul class="nav child_menu">
                                            <li><?= $this->Html->link('Search', ['controller' => 'Users', 'action' => 'candidates']); ?></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /sidebar menu -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <?= $loggedfirstname ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><?= $this->Html->link('Profile', ['controller' => 'users', 'action' => 'view']) ?></li>
                                        <li> <?= $this->Html->link('Log out', ['controller' => 'users', 'action' => 'logout']) ?></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                    <?= $this->Flash->render('auth') ?>
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>

                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Jobbörse
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div> 

        <?= $this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js') ?>
        <?= $this->Html->script('custom.min.js') ?>

        <?= $this->fetch('script') ?>
    </body>
</html>
