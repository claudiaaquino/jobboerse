<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Unternehmen - <?= h($unternehmen->name) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Name') ?></p>
                        <p><?= $unternehmen->name ? $unternehmen->name : 'keine Information'; ?></p>

                        <p class="title"><?= __('Email') ?></p>
                        <p><?= $unternehmen->email ? $unternehmen->email : 'keine Information'; ?></p>

                        <p class="title"><?= __('Address') ?></p>
                        <p><?= $unternehmen->address ? $unternehmen->address : 'keine Information'; ?></p>

                        <p class="title"><?= __('Zipcode') ?></p>
                        <p><?= $unternehmen->zipcode ? $unternehmen->zipcode : 'keine Information'; ?></p>

                        <p class="title"><?= __('Country') ?></p>
                        <p><?= $unternehmen->country ? $unternehmen->country : 'keine Information'; ?></p>

                        <p class="title"><?= __('State') ?></p>
                        <p><?= $unternehmen->state ? $unternehmen->state : 'keine Information'; ?></p>

                        <p class="title"><?= __('City') ?></p>
                        <p><?= $unternehmen->city ? $unternehmen->city : 'keine Information'; ?></p>

                        <p class="title"><?= __('Phone') ?></p>
                        <p><?= $unternehmen->phone ? $unternehmen->phone : 'keine Information'; ?></p>


                        <p class="title"><?= __('Created') ?></p>
                        <p><?= $unternehmen->created ? $unternehmen->created : 'keine Information'; ?></p>


                        <p class="title"><?= __('Updated') ?></p>
                        <p><?= $unternehmen->updated ? $unternehmen->updated : 'keine Information'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $unternehmen->status ? __('Aktiv') : __('Inaktiv'); ?></p>

                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Description') ?></p>
                        <p>  <?= $unternehmen->description ? $this->Text->autoParagraph(h($unternehmen->description)) : 'keine Information'; ?></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("zurück", $backlink, ['class' => "btn btn-default"]) ?>
                        <?php if ($unternehmen_id) { //only a company can see this options  ?>                                          
                            <?= $this->Html->link("ändern", ['action' => 'edit', $unternehmen->id], ['class' => "btn btn-primary"]) ?>
                            <?= $this->Form->postLink("löschen", ['action' => 'delete', $unternehmen->id], ['class' => "btn btn-danger", 'confirm' => __('Sind Sie sicher?', $unternehmen->id)]) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


