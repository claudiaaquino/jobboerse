<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Meine Datein ändern') ?> <small>* required</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($user, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>


                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('name', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('username', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('password', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('email', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         
                <?php if (!$unternehmen_id) { //only a bewerber can see this option       ?>

                    <div class="form-group well-sm well">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qualification_ids">Fähigkeiten und Kenntnisse
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('qualification_ids', ['options' => $qualifications, 'default' => $selectedqualifications, "class" => "form-control", "multiple" => "multiple", 'label' => false, "style" => "width: 100%"]); ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Addresse 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('address', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="zipcode">Postleitzahl 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('zipcode', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="country">Land
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('country', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state">Region
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('state', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">Stadt 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('city', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Telefonnummer 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('phone', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone">Handynummer
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('cellphone', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>  
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Ändern</button>
                        <?= $this->Html->link(__('zurück'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#qualification-ids").select2({tokenSeparators: [',', ';'], tags: true, multiple: true, placeholder: 'wenn es hier nicht gibt, schreib es einfach.'});
    });
</script>