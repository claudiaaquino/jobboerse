<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Triade Consultoria</title>
        <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('/vendors/animate.css/animate.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    

    </head>


    <body class="login">
        <div>
            <a class="hiddenanchor" id="signin"></a>
            <a class="hiddenanchor" id="signup"></a>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <?= $this->Form->create() ?>

                        <h1>Jobbörse System</h1>
                        <p><?= $this->Flash->render() ?></p>
                        <p><?= $this->Flash->render('auth') ?></p>
                        <div>
                            <?= $this->Form->input('username', ['label' => false, "class" => "form-control", "placeholder" => "Username"]) ?>
                            <?= $this->Form->input('password', ['label' => false, "class" => "form-control", "placeholder" => "Password"]) ?>
                        </div>
                        <div>
                            <?= $this->Form->button(__('Anmelden'), ["class" => "btn btn-default submit"]); ?>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">Bist du neu hier?
                                <a href="#signup" class="to_register"> Sign Up! </a>
                            </p>

                        </div>
                        <?= $this->Form->end() ?>
                    </section>
                </div>

                <div id="register" class="animate form registration_form">
                    <section class="login_content">
                        <?= $this->Form->create($user, ['url' => ['controller' => 'Users', 'action' => 'add']]); ?>
                        <h1>Jobbörse System</h1>
                        <div>
                            <input type="text" id="nome" name="name"  class="form-control" placeholder="Name" required="" />
                        </div>
                        <div>
                            <input type="text" id="username" name="username" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="email" id="email" name="email"  class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <button class="btn btn-default submit" type="submit">Weiter</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">Hast du schon einen Account? 
                                <a href="#signin" class="to_register">Dann logge dich hier ein </a>
                            </p>

                        </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
