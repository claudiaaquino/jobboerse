<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Profile</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($user->has('unternehmen')) { ?>
                            <p class="title"><?= __('Unternehmen') ?></p>
                            <p><?= $this->Html->link($user->unternehmen->name, ['controller' => 'Unternehmen', 'action' => 'view', $user->unternehmen->id]) ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Name') ?></p>
                        <p><?= $user->name ? $user->name : 'keine Information'; ?></p>

                        <p class="title"><?= __('Username') ?></p>
                        <p><?= $user->username ? $user->username : 'keine Information'; ?></p>


                        <p class="title"><?= __('Email') ?></p>
                        <p><?= $user->email ? $user->email : 'keine Information'; ?></p>

                        <p class="title"><?= __('Phone') ?></p>
                        <p><?= $user->phone ? $user->phone : 'keine Information'; ?></p>

                        <p class="title"><?= __('Cellphone') ?></p>
                        <p><?= $user->cellphone ? $user->cellphone : 'keine Information'; ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">


                        <p class="title"><?= __('Address') ?></p>
                        <p><?= $user->address ? $user->address : 'keine Information'; ?></p>

                        <p class="title"><?= __('Zipcode') ?></p>
                        <p><?= $user->zipcode ? $user->zipcode : 'keine Information'; ?></p>

                        <p class="title"><?= __('Country') ?></p>
                        <p><?= $user->country ? $user->country : 'keine Information'; ?></p>

                        <p class="title"><?= __('State') ?></p>
                        <p><?= $user->state ? $user->state : 'keine Information'; ?></p>

                        <p class="title"><?= __('City') ?></p>
                        <p><?= $user->city ? $user->city : 'keine Information'; ?></p>


                    </div>
                </div>


                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Lastlogin') ?></p>
                        <p><?= $user->lastlogin ? $user->lastlogin : 'keine Information'; ?></p>


                        <p class="title"><?= __('Created') ?></p>
                        <p><?= $user->created ? $user->created : 'keine Information'; ?></p>


                        <p class="title"><?= __('Updated') ?></p>
                        <p><?= $user->updated ? $user->updated : 'keine Information'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $user->status ? __('Aktiv') : __('Inaktiv'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("zurück", $backlink, ['class' => "btn btn-default"]) ?>
                        <?php if ($loggeduser == $user->id) { ?>
                            <?= $this->Html->link("ändern", ['action' => 'edit', $user->id], ['class' => "btn btn-primary"]) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($user->userqualifications)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Fähigkeiten und Kenntnisse </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Qualifikation') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($user->userqualifications as $userqualifications):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($userqualifications->qualification->description) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Form->postLink(__('löschen'), ['controller' => 'Userqualifications', 'action' => 'delete', $userqualifications->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Sind sie sicher, dass Sie es löschen möchten??', $userqualifications->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($user->applications) && ($loggeduser == $user->id)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Meine Bewerbung(en) </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Job Title') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Unternehmen') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Bewerbungsdatum') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($user->applications as $applications):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($applications->job->title) ?></td>
                                        <td><?= h($applications->job->unternehman->name) ?></td>
                                        <td><?= h($applications->created) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('zeigen'), ['controller' => 'Applications', 'action' => 'view', $applications->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('ändern'), ['controller' => 'Applications', 'action' => 'edit', $applications->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('löschen'), ['controller' => 'Applications', 'action' => 'delete', $applications->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Sind sie sicher, dass Sie es löschen möchten??', $applications->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


